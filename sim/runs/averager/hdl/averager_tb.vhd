library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity averager_tb is
end entity averager_tb;

architecture test_tb of averager_tb is

  constant c_width       : natural := 12;
  constant c_log2_window : natural := 7;

  signal clk, rst : std_logic                            := '1';
  signal samples  : std_logic_vector(c_width-1 downto 0) := (others => '0');
  signal average  : std_logic_vector(samples'range)      := (others => '0');

begin

  DUT : entity work.averager
    generic map (g_log2_window => c_log2_window)
    port map (clk     => clk,
              rst     => rst,
              samples => samples,
              average => average);

  clk <= not clk after 10 ns;           -- clock generation

  U_samples : process (clk) is
    use std.textio.all;
    file mysamples   : text open read_mode is "samples/samples.txt";
    variable values  : line;
    variable sample  : integer;
    variable read_ok : boolean;
  begin
    if rising_edge(clk) then
      if rst = '0' then
        readline(mysamples, values);
        read (values, sample, read_ok);
        samples <= std_logic_vector(to_signed(sample, 12));
      end if;
    end if;
  end process U_samples;

  WaveGen_Proc : process
  begin
    rst <= '1';
    wait for 200 ns;
    rst <= '0';
    wait;
  -- wait until clk = '1';
  end process WaveGen_Proc;

end architecture test_tb;

configuration averager_tb_test_tb_cfg of averager_tb is
  for test_tb
  end for;
end averager_tb_test_tb_cfg;
