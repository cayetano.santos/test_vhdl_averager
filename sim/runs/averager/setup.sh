#!/bin/sh

# Containing project with support files
SUPPORT=$HOME/Projects/perso/XilinxISE147_Makefile

[[ -e ghdl.sh ]] && rm ghdl.sh
ln -s $SUPPORT/ghdl.sh ghdl.sh

echo "Done."
