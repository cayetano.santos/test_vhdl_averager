#!/bin/sh

# create gtags.files with al vhd files in project
find ./src -type f \( -iname \*.vhd -o -iname \*.ucf \) -print > gtags.files
find ./sim/averager -type f \( -iname \*.vhd -o -iname \*.vhdl \) -print >> gtags.files

echo "Done."
