-- * Top package
--
--! @file
--! @brief Project package.
--!
--! Two constants are include here, one for defining samples number of bits,
--! and a second one defining the window width
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
--! \class top_pkg

-- * Libraries

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- * Package

--! @brief package contents.\n\n
--!
--! @details Package details. Does this yet work ?\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...

package top_pkg is

  -- ** constants

  --!\name Constants group 1
  --!\{
  --! number of bits in samples
  constant c_width     : natural := 12;
  constant c_unusued_1 : natural := 7;
  constant c_unusued_2 : natural := 7;
  --!\}

  --!\name Constants group 2
  --!\{
  --! log2() of window length
  constant c_log2_window : natural := 7;
  constant c_unusued_3   : natural := 7;
  constant c_unusued_4   : natural := 7;
  --!\}

end package top_pkg;
